/*
 * web: org.nrg.xnat.exceptions.MultipleScanException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.exceptions;


/**
 * @author Timothy R. Olsen <olsent@wustl.edu>
 *
 */
public class MultipleScanException extends Exception {

}
